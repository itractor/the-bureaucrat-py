from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets

from .serializers import StockSerializer
from .models import Stock


class StockViewSet(viewsets.ModelViewSet):
    queryset = Stock.objects.all().order_by('ticker')
    serializer_class = StockSerializer
